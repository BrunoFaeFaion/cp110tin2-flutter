import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'CP110TIN2 Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Scaffold(
        appBar: AppBar(title: Text('Home')),
        drawer: MyDrawer(),
        body: Home(),
      ),
    );
  }
}

class MyDrawer extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Text('Itens do Drawer'),
            decoration: BoxDecoration(
              color: Colors.blue,
            ),
          ),
          ListTile(
            title: Text('Tab 01'),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Tab01()),
              );
            },
          ),
          ListTile(
            title: Text('Tab 02'),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Tab02()),
              );
            },
          ),
        ],
      ),
    );
  }
}

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text('🏡', style: TextStyle(fontSize: 50),),
      ),
    );
  }
}

class Tab01 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Tab 01')),
      body: Center(
        child: Text('🏰', style: TextStyle(fontSize: 50),),
      ),
    );
  }
}

class Tab02 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Tab 02')),
      body: Center(
        child: Text('🕌', style: TextStyle(fontSize: 50),),
      ),
    );
  }
}
