# Desenvolvimento de Aplicações para Mobile (CP110TIN2) - Flutter

Projeto base para a aula de Flutter da matéria Desenvolvimento de Aplicações para Mobile.

## 🔎 Sobre
App simples com alguns elementos básicos, como **AppBar**, **Drawer** e conceitos de **Navegação**.
##### Árvore de Widgets
```
MaterialApp
|-- AppBar
|-- Drawer
|   |-- Drawer Item: Tab 01 (Text)
|   |-- Drawer Item: Tab 02 (Text)
|-- Home
|   |-- Unicode Icon 🏡 (Text)
|-- Tab01
|   |-- Unicode Icon 🏰 (Text)
|-- Tab02
|   |-- Unicode Icon 🕌 (Text)
```
A árvore de widgets acima é puramente conceitual e simplificada para fins educativos.

## 📖 Guias

Instruções para utilizar o projeto.

##### Requisitos
- [Flutter](https://flutter.dev/docs/get-started/install)

##### Testando
O projeto pode ser testado via **Android Studio** (/**VSCode** com pacotes instalados) através da interface gráfica ou **CLI** (linhas de comando) utilizando o comando `flutter run` dentro do diretório do projeto.
